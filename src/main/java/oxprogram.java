
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author admin02
 */
public class oxprogram {

    static char winner = '-';
    static int count=0;
    static boolean isFinish = false;
    static int row, col;
    static Scanner sc = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("welcome to OX Game.");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");

        }

    }

    static void showTurn() {
        System.out.println(player + " turn");

    }

    static void input() {
        //Normol flow
        while (true) {
            System.out.println("Please input Row Col");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;

            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            } if(count>= 8){
                break;
            }
            System.out.println("Error table at row and col is not empty!!!");

        }
        showTable();
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkdiagonally() {
          for (int i=0; i<3 ; i++){
              for(int j=0; j<3; j++){
                  if(table[j][j]!= player){
                      return;
                  }
              }
          }
        isFinish = true;
        winner = player;
        
       
    }
     static void checkdiagonally1() {
          for (int i=0; i<3 ; i++){
              for(int j=0,k=2;j<3; j++,k--){
                  if(table[j][k]!= player){
                      return;
                  }
              }
          }
        isFinish = true;
        winner = player;
        
       
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkdiagonally();
        checkdiagonally1();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        } count++;
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");

        } else {
            System.out.println(winner + " Win!!");
        }
    }

    static void checkDraw() {
        if(count >= 8){
            winner= '-';
            isFinish = true;
        }
    }

    static void showBye() {
        System.out.println("Bye bye....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

}
